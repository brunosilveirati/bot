import argparse
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from random import randint
import json
import logging
import time
from time import sleep
from queue import Queue, Empty
import threading
from threading import Thread, Lock
import os

__x_path_links = "//div[@class='card-body d-flex flex-column']//a[@class='btn btn-lg btn-block btn-outline-primary']"
__path_json_config = "config.json"

global configBot, logger, linksProducts
mutex = Lock()

class Info:
    def __init__(self, name):
        self.__name = name
        self.__isUsed = False

    def set_name(self, name):
        self.__name = name
    
    def get_name(self):
        return self.__name

    def set_used(self, used):
        self.__isUsed = used
    
    def is_used(self):
        return self.__isUsed

class ThreadInfo:
    def __init__(self, count):
        self.__infos = []

        for i in range(0, count):        
            self.__infos.append(Info(i))

    def get_info(self):
        for info in self.__infos:
            if not info.is_used():
                info.set_used(True)
                return info
        
        return None

class Worker(Thread):
    _TIMEOUT = 2
    """ Thread executing tasks from a given tasks queue. Thread is signalable, 
        to exit
    """
    def __init__(self, tasks, th_num):
        Thread.__init__(self)
        self.tasks = tasks
        self.daemon, self.th_num = True, th_num
        self.done = threading.Event()
        self.start()

    def run(self):       
        while not self.done.is_set():
            try:
                func, args, kwargs = self.tasks.get(block=True,
                                                   timeout=self._TIMEOUT)
                try:
                    func(*args, **kwargs)
                except Exception as e:
                    print(e)
                finally:
                    self.tasks.task_done()
            except Empty as e:
                pass
        return

    def signal_exit(self):
        """ Signal to thread to exit """
        self.done.set()

class ThreadPool:
    """Pool of threads consuming tasks from a queue"""
    def __init__(self, num_threads, tasks=[]):
        self.tasks = Queue(num_threads)
        self.workers = []
        self.done = False
        self._init_workers(num_threads)
        for task in tasks:
            self.tasks.put(task)

    def _init_workers(self, num_threads):
        for i in range(num_threads):
            self.workers.append(Worker(self.tasks, i))

    def add_task(self, func, *args, **kwargs):
        """Add a task to the queue"""
        self.tasks.put((func, args, kwargs))

    def _close_all_threads(self):
        """ Signal all threads to exit and lose the references to them """
        for workr in self.workers:
            workr.signal_exit()
        self.workers = []

    def wait_completion(self):
        """Wait for completion of all the tasks in the queue"""
        self.tasks.join()

    def __del__(self):
        self._close_all_threads()

class ConfigBot:
    def __init__(self, urlSite, urlCheckout, urlProducts, pathLog, urlFakePerson, urlFakeCC, maxThread, outputName):
        self.__urlSite = urlSite
        self.__urlCheckout = urlCheckout
        self.__urlProducts = urlProducts
        self.__pathLog = pathLog
        self.__urlFakePerson = urlFakePerson
        self.__urlFakeCC = urlFakeCC
        self.__maxThread = maxThread
        self.__outputName = outputName

    def get_max_thread(self):
        return self.__maxThread

    def get_url_site(self):
        return self.__urlSite

    def get_url_products(self):
        return self.__urlProducts
    
    def get_url_checkout(self):
        return self.__urlCheckout

    def get_path_log(self):
        return self.__pathLog

    def get_url_fake_person(self):
        return self.__urlFakePerson

    def get_url_fake_cc(self):
        return self.__urlFakeCC

    def get_output_name(self):
        return self.__outputName

class Person:
    def __init__(self, firstName, lastName, email, address, state, complement, zipCode, ccName, ccNumber, ccExpiration, ccCVV):        
        self.__firstName = firstName
        self.__lastName = lastName
        self.__email = email
        self.__address = address
        self.__state = state
        self.__complement = complement
        self.__zipCode = zipCode
        self.__ccName = ccName
        self.__ccNumber = ccNumber
        self.__ccExpiration = ccExpiration
        self.__ccCVV = ccCVV

    def getFirstName(self):
        return self.__firstName

    def setFirstName(self, val):
        self.__firstName = val

    def getLastName(self):
        return self.__lastName

    def setLastName(self, val):
        self.__lastName = val

    def getEmail(self):
        return self.__email

    def setEmail(self, val):
        self.__email = val

    def getAddress(self):
        return self.__address

    def setAddress(self, val):
        self.__address = val

    def getState(self):
        return self.__state

    def setState(self, val):
        self.__state = val

    def getComplement(self):
        return self.__complement

    def setComplement(self, val):
        self.__complement = val

    def getZipCode(self):
        return self.__zipCode

    def setZipCode(self, val):
        self.__zipCode = val

    def getCcName(self):
        return self.__ccName

    def setCcName(self, val):
        self.__ccName = val

    def getCcNumber(self):
        return self.__ccNumber

    def setCcNumber(self, val):
        self.__ccNumber = val

    def getCcExpiration(self):
        return self.__ccExpiration

    def setCcExpiration(self, val):
        self.__ccExpiration = val

    def getCcCVV(self):
        return self.__ccCVV

    def setCcCVV(self, val):
        self.__ccCVV = val

def get_message(message):
    localtime = time.asctime( time.localtime(time.time()) )
    return message + ": " + localtime

# get all product links
def get_all_products_links(info):    
    mutex.acquire()
    global linksProducts
    driver = webdriver.Firefox()
    linksProducts = []
    logger.info(get_message("Start-Get-Links-Products"))
    try:
        driver.get(configBot.get_url_products())
        elementsLinks = driver.find_elements_by_xpath(__x_path_links)        

        if len(elementsLinks) > 0:
            for link in elementsLinks:
                product_link = link.get_attribute("href")
                logger.info(get_message("Find-link: " + str(product_link)))
                linksProducts.append(product_link)    
    except Exception as e:
        logger.error(get_message("Error-Get-Links-Products: " + str(e)))  
    finally:        
        driver.quit()
        mutex.release()
        info.set_used(False)
        logger.info(get_message("Finish-Get-Links-Products"))

def create_person(driver): 
    try:        
        driver.get(configBot.get_url_fake_person())
        sexs = driver.find_elements_by_name("sexo")
        randomSex = randint(0, len(sexs)-1)
        sexs[randomSex].click()

        driver.find_element_by_id('bt_gerar_pessoa').click()    

        sleep(1)

        firstName = driver.find_element_by_xpath("//div[@id='nome']//span").get_attribute('innerHTML')
        nameSplit = firstName.split(" ")
        firstName = nameSplit[0]
        nameSplit.pop(0)
        lastName = " ".join(nameSplit)
        email = driver.find_element_by_xpath("//div[@id='email']//span").get_attribute('innerHTML')
        address = driver.find_element_by_xpath("//div[@id='endereco']//span").get_attribute('innerHTML')        
        state = driver.find_element_by_xpath("//div[@id='estado']//span").get_attribute('innerHTML')
        zipCode = driver.find_element_by_xpath("//div[@id='cep']//span").get_attribute('innerHTML')
        complement = "" 

        driver.get(configBot.get_url_fake_cc())    
        driver.find_element_by_id('bt_gerar_cc').click()

        sleep(1)

        ccName = firstName + " " + lastName
        ccNumber = driver.find_element_by_id('cartao_numero').get_attribute('innerHTML').replace('<span class="clipboard-copy"></span>', "").replace(" ", "")
        ccExpiration = driver.find_element_by_id('data_validade').get_attribute('innerHTML').replace('<span class="clipboard-copy"></span>', "")
        ccCVV = driver.find_element_by_id('codigo_seguranca').get_attribute('innerHTML').replace('<span class="clipboard-copy"></span>', "")

        return Person(firstName, lastName, email, address, state, complement, zipCode, ccName, ccNumber, ccExpiration, ccCVV)
    except Exception as e:        
        logger.error(get_message("Error-Generate-Fake-Person" + str(e)))

    return None

# here buy random procut for each person
def buy_product_in_store(info):
    logger.info(get_message("Thread-" + str(info.get_name()) + "-Start-Buy-Products"))
    try:
        driver = webdriver.Firefox()
        p = create_person(driver)
        driver.delete_all_cookies()
        # get random link
        randomLink = randint(0, len(linksProducts)-1)
        urlProduct = linksProducts[randomLink]            
        logger.info(get_message(p.getFirstName() + "-" + p.getLastName() + "-Buy-Product: " + urlProduct))
        # choose product        
        driver.get(urlProduct)
        driver.find_element_by_class_name("btn-primary").click()
        sleep(2)
        driver.get(configBot.get_url_checkout())
        # before choose product go to checkout and fill form
        firstName = driver.find_element_by_id("firstName")
        lastName = driver.find_element_by_id("lastName")
        email = driver.find_element_by_id("email")
        address = driver.find_element_by_id("address")
        state = driver.find_element_by_id("state")
        zipCode = driver.find_element_by_id("zip")
        ccName = driver.find_element_by_id("cc-name")
        ccNumber = driver.find_element_by_id("cc-number")
        ccExpiration = driver.find_element_by_id("cc-expiration")
        ccCvv = driver.find_element_by_id("cc-cvv")
        complement =  driver.find_element_by_id("complement")

        firstName.send_keys(p.getFirstName())
        lastName.send_keys(p.getLastName())
        email.send_keys(p.getEmail())
        address.send_keys(p.getAddress())
        complement.send_keys(p.getComplement())
        state.send_keys(p.getState())
        zipCode.send_keys(p.getZipCode())
        ccName.send_keys(p.getCcName())
        ccNumber.send_keys(p.getCcNumber())
        ccExpiration.send_keys(p.getCcExpiration())
        ccCvv.send_keys(p.getCcCVV())

        # submit form and close driver
        driver.find_element_by_class_name("btn-block").click()
        driver.quit()
        mutex.acquire()
        save_data(p)
    except Exception as e:
        logger.error(get_message("Thread-" + str(info.get_name()) + "-Error-Buy-Product: " + str(e)))  
    finally:       
        mutex.release() 
        info.set_used(False)
        logger.info(get_message("Thread-" + str(info.get_name()) + "-Finish-Buy-Products"))

def read_config_json():
    global configBot

    with open(__path_json_config) as f:
        data = json.load(f)        
        configBot = ConfigBot(data["url_site"], data["url_checkout"], data["url_products"], data["path_file_log"], data["ult_fake_person"], data["url_fake_cc"], data["max_thread"], data["output_name"])

def save_data(person):
    data = []

    if not os.path.isfile(configBot.get_output_name()) or os.stat(configBot.get_output_name()).st_size == 0:
        data.append(person.__dict__)        
    else:
        data = write_json()
        data.append(person.__dict__)

    save_json(data)

def write_json():
    with open(configBot.get_output_name()) as f:
        return json.load(f) 

def save_json(data):
    with open(configBot.get_output_name(), 'w') as outfile:
        json.dump(data, outfile, indent=4)

def start(qtd):
    read_config_json()

    if configBot is None:
        return

    global logger
    logging.basicConfig(filename=configBot.get_path_log(),level=logging.INFO)
    logger = logging.getLogger("BOT")
    logger.info(get_message("Start-Bot"))
    maxThread = qtd >= configBot.get_max_thread() and configBot.get_max_thread() or qtd

    try:
        pool = ThreadPool(maxThread)
        infos = ThreadInfo(maxThread)

        info = infos.get_info()                
        get_all_products_links(info)        
        count = 0
        while True:
            for i in range(0, maxThread):                
                info = infos.get_info()
                if not info == None:                
                    count += 1
                    pool.add_task(buy_product_in_store, info)
            pool.wait_completion()

            if count >= qtd:
                break
    except Exception as e:
        logger.error(get_message("Error-Bot: " + str(e)))
    finally:   
        logger.info(get_message("Finish-Bot"))
    
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Attack Store')
    parser.add_argument('--q', type=int, dest="qtd", required=True)
    args = parser.parse_args()
    start(args.qtd)